/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.midtermoverload;

/**
 *
 * @author bwstx
 */
public class Introduce {
    public void print(){
        System.out.println("Hello !");
    }
    public void print(String fname, String lname){
        System.out.println("My name is "+fname+" "+lname);
    }
    public void print(int age){
        System.out.println("I'm "+age+" years old !");
    }
    public void print(double grade){
        System.out.printf("My GPA is %.2f\n ",grade);
    }
    
}
